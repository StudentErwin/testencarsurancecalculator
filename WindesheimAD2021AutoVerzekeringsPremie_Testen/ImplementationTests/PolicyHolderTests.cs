﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;

namespace WindesheimAD2021AutoVerzekeringsPremie_Testen.ImplementationTests
{
    public class PolicyHolderTests
    { 
        [Fact]
        public void LicentieBerekeningTest()
        {
            //Arrange
            var age = 1;
            var driverLicenceStartDate = "1-1-2000";
            var postalCode = 1111;
            var noClaimYears = 0;
            var expected = DateTime.Today.Year - 2000;

            //Act
            var policyHolder = new PolicyHolder(age, driverLicenceStartDate, postalCode, noClaimYears);

            //Assert
            Assert.Equal(expected, policyHolder.LicenseAge);
        }
    }
}
