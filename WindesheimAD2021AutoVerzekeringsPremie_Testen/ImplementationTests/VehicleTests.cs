﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using Xunit;
using Xunit.Sdk;

namespace WindesheimAD2021AutoVerzekeringsPremie_Testen.ImplementationTests
{
    public class VehicleTests
    {
        [Theory]
        [InlineData(20, 20)]
        [InlineData(0, 0)]
        [InlineData(-20, 0)]
        public void AutoLeeftijdBerekeningTest(int age, int expected)
        {
            //Arrange
            var powerInKw = 1;
            var valueInEuros = 1;
            var constructionYear = DateTime.Now.Year - age;

            //Act
            var vehicle = new Vehicle(powerInKw, valueInEuros, constructionYear);

            //Assert
            Assert.Equal(expected, vehicle.Age);
        }
    }
}
