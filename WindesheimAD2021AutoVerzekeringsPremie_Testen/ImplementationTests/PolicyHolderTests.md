﻿##PolicyHolderTests##

###LicentieBerekeningTest###
Doel: Kijken of de berekening van de licentie leeftijd goed gaat.

Data: Het maakt niet uit hoeveel jaar maar de dag en maand moet 1 januari zijn. Anders kan er gebeuren dat er -1 jaar gedaan wordt en faalt de test.

Techniek:
  - Fact, want het hoeft maar 1 keer getest te worden.

Fouten:
 - Geen fouten gevonden.