﻿##PremiumCalculationTests##

###WaardeBerekeningZonderOpslagOfKortingTest###
Doel: Om te kijken of de berekening van de basis premie wel goed werkt.

Data: Het maakt bij deze niet uit welke data ik gebruik.

Techniek:
 - Fact, omdat maar 1 keer data ingevuld hoeft te worden.
 - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - De mock werkt niet als de data niet virtual is. Dus daarom heb ik de ints naar virtual veranderd.
 - Ik verwachte dat het antwoord 34,67 (afgerond met 2 cijfers achter de komma voor de vergelijking), want dat was mijn eigen berekening met de requirements. 
   Dit was het niet omdat deze () niet in de bereking gebruikt werd. Daarnaast gaf het geen double mee maar een int.


###PremieOpslagOnderLeeftijd23_OfKorterDan5JaarRijbewijsTest###
Doel: De premie moet met 15% verhoogt worden bij een leeftijd onder 23 of bij een rijbewijs leeftijd korter dan 5 jaar.

Data: Deze data zijn alle grenswaardes van leeftijd en rijbewijs leeftijd. Plus nog 1 die bij beide een opslag krijgt om te kijken of het geen extra opslag geeft.

Techniek:
  - Theorie, want meerdere grenswaardes van age en licenseAge moet gecontroleerd worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - In de code staat een kleiner dan of een gelijk aan teken (<=) bij de if statement van de rijbewijs leeftijd. In de requirements staat alleen kleiner dan, dus '<=' moet veranderen naar '<'.
 - De mock werkt niet als de data niet virtual is. Dus daarom heb ik de ints naar virtual veranderd.


###PostcodeOpslagTest###
Doel: Bij een postcode van 10xx t/m 35xx moet de risico opslag 5% zijn en bij 36xx t/m 44xx moet het 2%. Daarnaast moet het niet verhogen bij een andere getal.

Data: Deze data zijn alle grenswaardes van de postcodes.

Techniek:
  - Theorie, want meerdere grenswaardes moeten gecontroleerd worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.


###PremieOpslagBijBeterePremiesTest###
Doel: De betere premies zijn duurder, dus de test moet controleren of het wel echt verhoogd wordt.

Data: De data dat anders is de WA plus en All Rick.

Techniek:
  - Fact, omdat de data niet via een theorie goed gedaan kon worden vanwege de protection level van InsuranceCoverage.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.


###SchadeVrijeJarenKortingTest###
Doel: De betere premies zijn duurder, dus de test moet controleren of het wel echt verhoogd wordt.

Data: De data zijn alle grenswaardes van schadevrije jaren.

Techniek:
  - Theorie, want meerdere grenswaardes moeten gecontroleerd worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Bij UpdatePremiumForNoClaimYears() was NoClaimPercentage een int waardoor de berekening van premium niet goed ging. Dit heb ik ontdenkt doordat de premium bij meerdere grenswaardes 0 was.


###JaarBetalingKortingTest###
Doel: Bij een premie betalen per jaar moet er een korting zijn van 2.5%.

Data: De data is niet van belang, maar de expected moet wel een hele getal zijn. 
      Dit is omdat de premiumPaymentAmount al afgerond wordt voordat *1.025 gebeurd, waardoor een klein verschil ontstaat achter de komma.

Techniek:
  - Fact, omdat maar het maar 1 keer gedaan hoeft te worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.


###JaarBetalingKortingTest###
Doel: In de requirements staat dat de premie met twee cijfers afgerond moet worden.

Data: Het maakt bij deze niet uit welke data ik gebruik.

Techniek:
  - Fact, omdat maar het maar 1 keer gedaan hoeft te worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.


###PremieWordtAfgerondNaar2CijfersAchterDeKommaTest###
Doel: In de requirements staat dat de premie met twee cijfers afgerond moet worden.

Data: Het maakt bij deze niet uit welke data ik gebruik.

Techniek:
  - Fact, omdat maar het maar 1 keer gedaan hoeft te worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.


###MaandBerekeningTest###
Doel: Het omzetten van de jaarpremie naar maandpremie moet goed gebeuren.

Data: Het maakt bij deze niet uit welke data ik gebruik.

Techniek:
  - Fact, omdat maar het maar 1 keer gedaan hoeft te worden.
  - Mock, want anders doet de test het volgend jaar niet meer.

Fouten:
 - Geen fouten gevonden.