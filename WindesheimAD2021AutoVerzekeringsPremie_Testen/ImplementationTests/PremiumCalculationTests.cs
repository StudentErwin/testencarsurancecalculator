using System;
using Microsoft.VisualBasic;
using Moq;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace WindesheimAD2021AutoVerzekeringsPremie_Testen.ImplementationTests
{
    public class PremiumCalculationTests
    {
        [Fact]
        public void WaardeBerekeningZonderOpslagOfKortingTest()
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var expected = 34.67;

            //Act
            var premiumCalculation = PremiumCalculation.CalculateBasePremium(mockVehicle.Object);

            //Assert
            Assert.Equal(expected, Math.Round(premiumCalculation, 2));
        }

        [Theory]
        [InlineData(10, 22, 115)]
        [InlineData(10, 23, 100)]
        [InlineData(10, 24, 100)]
        [InlineData(4, 30, 115)]
        [InlineData(5, 30, 100)]
        [InlineData(6, 30, 100)]
        [InlineData(4, 22, 115)]

        public void PremieOpslagOnderLeeftijd23_OfKorterDan5JaarRijbewijsTest(int licenseAge, int age, int expected)
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder(licenseAge: licenseAge, age: age);
            var standardPremium = 34.66;

            //Act
            var premiumCalculation = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA);

            var secondPremium = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(secondPremium / standardPremium * 100));
        }

        [Theory]
        [InlineData(1000, 105)]
        [InlineData(3400, 105)]
        [InlineData(3500, 105)]
        [InlineData(3600, 102)]
        [InlineData(4300, 102)]
        [InlineData(4400, 102)]
        [InlineData(4500, 100)]
        public void PostcodeOpslagTest(int postalCode, int expected)
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder(postalCode: postalCode);
            var standardPremium = 34.66;

            //Act
            var premiumCalculation = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA);

            var secondPremium = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(secondPremium / standardPremium * 100));
        }

        [Fact]
        public void PremieOpslagBijBeterePremiesTest()
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder();
            var standardPremium = 34.66;
            var plusExpected = 120;
            var allRiskExpected = 200;

            //Act
            var plusPremiumCalculation = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA_PLUS);
            var allRiskPremiumCalculation = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.ALL_RISK);

            var plusPremium = Math.Round(plusPremiumCalculation.PremiumAmountPerYear, 2);
            var allRiskPremium = Math.Round(allRiskPremiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(plusExpected, Math.Round(plusPremium / standardPremium * 100));
            Assert.Equal(allRiskExpected, Math.Round(allRiskPremium / standardPremium * 100));
        }

        [Theory]
        [InlineData(5, 100)]
        [InlineData(6, 95)]
        [InlineData(7, 90)]
        [InlineData(17, 40)]
        [InlineData(18, 35)]
        [InlineData(19, 35)]
        public void SchadeVrijeJarenKortingTest(int noClaimYears, int expected)
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder(noClaimYears: noClaimYears);
            var standardPremium = 34.66;

            //Act
            var premiumCalculation = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA);

            var secondPremium = Math.Round(premiumCalculation.PremiumAmountPerYear, 2);

            //Assert
            Assert.Equal(expected, Math.Round(secondPremium / standardPremium * 100));
        }

        [Fact]
        public void JaarBetalingKortingTest()
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder();
            var expected = 35;

            //Act
            var premiumPaymentAmount = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.Equal(expected, Math.Round(premiumPaymentAmount * 1.025));
        }

        [Fact]
        public void PremieWordtAfgerondNaar2CijfersAchterDeKommaTest()
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder();

            //Act
            var premiumPaymentAmount = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR);

            //Assert
            Assert.True(premiumPaymentAmount * 100 % 1 == 0);
        }

        [Fact]
        public void MaandBerekeningTest()
        {
            //Arrange
            var mockVehicle = MockVehicle();
            var mockPoliceHolder = MockPolicyHolder();
            var expected = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR) / 12;

            //Act
            var premiumPaymentAmountMonth = new PremiumCalculation(mockVehicle.Object, mockPoliceHolder.Object, InsuranceCoverage.WA).PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.MONTH);

            //Assert
            Assert.Equal(Math.Round(expected), Math.Round(premiumPaymentAmountMonth));
        }

        private Mock<Vehicle> MockVehicle()
        {
            var mockVehicle = new Mock<Vehicle>(MockBehavior.Default, 0, 0, 0);
            mockVehicle.Setup(vehicle => vehicle.ValueInEuros).Returns(10000);
            mockVehicle.Setup(vehicle => vehicle.Age).Returns(6);
            mockVehicle.Setup(vehicle => vehicle.PowerInKw).Returns(50);
            return mockVehicle;
        }

        private Mock<PolicyHolder> MockPolicyHolder(int licenseAge = 10, int age = 30, int postalCode = 5555, int noClaimYears = 0)
        {
            var mockPoliceHolder = new Mock<PolicyHolder>(MockBehavior.Default, 0, "1-1-2021", 0, 0);
            mockPoliceHolder.Setup(holder => holder.LicenseAge).Returns(licenseAge);
            mockPoliceHolder.Setup(holder => holder.NoClaimYears).Returns(noClaimYears);
            mockPoliceHolder.Setup(holder => holder.PostalCode).Returns(postalCode);
            mockPoliceHolder.Setup(holder => holder.Age).Returns(age); 
            return mockPoliceHolder;
        }
    }
}
