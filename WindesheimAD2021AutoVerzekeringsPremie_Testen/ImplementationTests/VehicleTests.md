﻿##VehicleTests##

###LeeftijdBerekeningTest###
Doel: Kijken of de berekening van de leeftijd van de auto goed gaat.

Data: Ik heb gekozen voor een constructionYear van 20 jaar in de toekomst en 20 jaar in het verleden. 
      Het aantal jaar maakt eigenlijk niet uit maar het is belangrijk dat in ieder geval 1 normale en 1 auto uit de toekomst getest wordt.

Techniek:
  - Theorie, omdat twee constructionYears getest moet worden.

Fouten:
 - Geen fouten gevonden.